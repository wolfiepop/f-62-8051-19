// ----------------------------------------------------------------------------
// Nombre del Programa : ClienteServicio
// Autor               : Manuel Villalobos
// Compania            : GSOF
// Proyecto/Procliente : D-52-8272-17                    Fecha: 02/ABR/2018
// Descripcion General : CLASE SERVICIO PARA CLIENTE
// Programa Dependiente: N/A
// Programa Subsecuente: N/A
// Cond. de ejecucion  : N/A
// Dias de ejecucion   : N/A                                 Horario: N/A
//                              MODIFICACIONES
// ----------------------------------------------------------------------------
// Numero de Parametros:
// Parametros Entrada  :                                    Formato:
//
// Parametros Salida   : N/A                                Formato: N/A
// ----------------------------------------------------------------------------

package mx.com.prosa.wsca.servicio;

import mx.com.prosa.wsca.beans.*;
import mx.com.prosa.wsca.modelo.*;
import mx.com.prosa.wsca.util.*;
import mx.com.prosa.wsca.fabrica.*;
import java.util.*;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;
import java.util.stream.Collectors;
import java.io.File;
import java.io.IOException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

/**
 * Clase Servicio para Cliente
 * @author Manuel Villalobos (GSOF)
 * @version 02/ABR/2018
 */
public class ClienteServicio extends BaseServicio implements IClienteServicio {
    private static  ClienteServicio        instancia            = null;
    private         IClienteDAO            dao                  = ClienteFabrica.getDAO();
    private         EncriptadorPropago     encriptadorPropago   = AyudasFabrica.getEncriptadorPropago();

    /**
     * Constructor privado
     */
    private ClienteServicio() {
        setNombre("Clientes");
    }

    /**
     * Metodo para obtener la instancia del Singleton
     * @return La instancia del singleton
     */
    public static ClienteServicio getInstancia() {
        if(instancia == null) {
            instancia = new ClienteServicio();
        }

        return instancia;
    }

    /**
     * Metodo para obtener el contenido del catalogo
     * @param usuario  El usuario que ejecuta el metodo
     * @return Lista de elementos de tipo NotificacionObj
     */
    public List listar(final String usuario) {
        // INVOCA LA ACCION EN EL DAO, DEVUELVE LO QUE DA EL DAO
        return dao.listar(usuario);
    }

    /**
     * Metodo para obtener el contenido del catalogo de una afiliacion
     * Se deja para la generacion de reportes PDF y Excel
     * @param afiliacion    La afiliacion propietaria
     * @param usuario       El usuario que ejecuta el metodo
     * @return La lista de elementos
     */
    public List listar(final String afiliacion, final String usuario) {
        List<ClienteObj> lista;

        // SI HAY AFILIACION, CARGA SUS DATOS
        if(afiliacion != null){
            lista = dao.listar(afiliacion, usuario);
        // EN OTRO CASO CARGA COMPLETA
        }else{
            lista = dao.listar(usuario);
        }

        //A LA LISTA LE APLICA LA DESENCRIPCION DE CADA ELEMENTO
        for (ClienteObj cliente : lista){
            cliente.setNmCliente(        encriptadorPropago.desencripta(cliente.getNmCliente()));
            cliente.setNmPaterno(        encriptadorPropago.desencripta(cliente.getNmPaterno()));
            cliente.setNmMaterno(        encriptadorPropago.desencripta(cliente.getNmMaterno()));
            cliente.setNoCuenta(         utilerias.enmascararTarjeta(cliente.getNoCuenta()));

            // PONE LA DESCRIPCION DE SI ESTA ACTIVO O NO
            if("1".compareTo(cliente.getCvActivo()) == 0){
                cliente.setCvActivo("SI");
            }else{
                cliente.setCvActivo("NO");
            }
        }
        return lista;
    }

    /**
     * Metodo para obtener el total de elementos que cumplen con los criterios
     * @param afiliacion    La afiliacion propietaria
     * @param usuario       Filtro usuario
     * @return Lista de elementos de tipo EventoObj
     */
    public int totales(final String afiliacion, final String usuario){
        // INVOCA LA ACCION EN EL DAO, DEVUELVE LO QUE DA EL DAO
        return dao.totales(afiliacion, usuario);
    }

    /**
     * Metodo para obtener el contenido del catalogo, paginado
     * @param afiliacion    La afiliacion propietaria
     * @param usuario       Filtro usuario
     * @param pagina        El numero de paginas
     * @param bloque        El tamanio el bloque
     * @return Lista de elementos de tipo EventoObj
     */
    public List detalles(final String afiliacion, final String usuario, final int pagina, final int bloque){
        List<ClienteObj> lista;

        // SI HAY AFILIACION, CARGA SUS DATOS
        if(afiliacion != null){
            lista = dao.detalles(afiliacion, usuario, pagina, bloque);
        // EN OTRO CASO CARGA COMPLETA
        }else{
            lista = dao.listar(usuario);
        }

        //A LA LISTA LE APLICA LA DESENCRIPCION DE CADA ELEMENTO
        for (ClienteObj cliente : lista){
            cliente.setNmCliente(        encriptadorPropago.desencripta(cliente.getNmCliente()));
            cliente.setNmPaterno(        encriptadorPropago.desencripta(cliente.getNmPaterno()));
            cliente.setNmMaterno(        encriptadorPropago.desencripta(cliente.getNmMaterno()));
            cliente.setNoCuenta(         utilerias.enmascararTarjeta(cliente.getNoCuenta()));
        }
        return lista;
    }

    /**
     * Metodo para crear un registro
     * @param cliente  Los datos del registro
     * @param usuario  El usuario que ejecuta el metodo
     * @return Si se crea correctamente
     */
    public boolean crear(final ClienteObj cliente, final String usuario){
        // ENCRIPTA EL NOMBRE Y APELLIDOS DEL CLIENTE

        cliente.setNmCliente(encriptadorPropago.encripta(cliente.getNmCliente()));
        cliente.setNmPaterno(encriptadorPropago.encripta(cliente.getNmPaterno()));
        cliente.setNmMaterno(encriptadorPropago.encripta(cliente.getNmMaterno()));
        cliente.setFultmod(  utilerias.calendarAFecha(utilerias.fechaActual()));
        cliente.setUultmod(  usuario);
        cliente.setPultmod(  "WSCA");

        // INVOCA LA ACCION EN EL DAO, DEVUELVE LO QUE DA EL DAO
        return dao.crear(cliente, usuario);
    }

    /**
     * Metodo para cargar un registro
     * @param llave    La llave del registro
     * @param usuario  El usuario que ejecuta el metodo
     * @return El registro
     */
    public ClienteObj cargar(final PKClienteObj llave, final String usuario){
        ClienteObj objeto = (ClienteObj)dao.cargar(llave, usuario);

        // INVOCA LA ACCION EN EL DAO, DEVUELVE LO QUE DA EL DAO CON NOMBRE Y APELLIDOS DESENCRIPTADOS
        objeto.setNmCliente(        encriptadorPropago.desencripta(objeto.getNmCliente()));
        objeto.setNmPaterno(        encriptadorPropago.desencripta(objeto.getNmPaterno()));
        objeto.setNmMaterno(        encriptadorPropago.desencripta(objeto.getNmMaterno()));
        return objeto;
    }

    /**
     * Metodo para actualizar un registro
     * @param datos    Los datos del registro
     * @param usuario  El usuario que ejecuta el metodo
     * @return Si se actualiza correctamente
     */
    public boolean actualizar(final ClienteObj datos, final String usuario){
        ClienteObj objeto = (ClienteObj)dao.cargar(datos.getLlave(), usuario);

        // COLOCA LOS DATOS, ENCRIPTANDO EL NOMBRE Y APELLIDOS
        objeto.setLlave(            datos.getLlave());
        objeto.setNmCliente(        encriptadorPropago.encripta(datos.getNmCliente()));
        objeto.setNmPaterno(        encriptadorPropago.encripta(datos.getNmPaterno()));
        objeto.setNmMaterno(        encriptadorPropago.encripta(datos.getNmMaterno()));
        objeto.setNoCuenta(         datos.getNoCuenta());
        objeto.setMtDefault(        datos.getMtDefault());
        objeto.setFeIniContrato(    datos.getFeIniContrato());
        objeto.setFeFinContrato(    datos.getFeFinContrato());
        objeto.setNumeroContrato(   datos.getNumeroContrato());
        objeto.setCvActivo(         datos.getCvActivo());
        objeto.setGeCampo1(         datos.getGeCampo1());
        objeto.setGeCampo2(         datos.getGeCampo2());
        objeto.setGeCampo3(         datos.getGeCampo3());
        objeto.setGeCampo4(         datos.getGeCampo4());
        objeto.setGeCampo5(         datos.getGeCampo5());
        objeto.setGeCampo6(         datos.getGeCampo6());
        objeto.setGeCampo7(         datos.getGeCampo7());
        objeto.setGeCampo8(         datos.getGeCampo8());
        objeto.setGeCampo9(         datos.getGeCampo9());
        objeto.setGeCampo10(        datos.getGeCampo10());
        objeto.setGeCampo11(        datos.getGeCampo11());
        objeto.setGeCampo12(        datos.getGeCampo12());
        objeto.setGeCampo13(        datos.getGeCampo13());
        objeto.setGeCampo14(        datos.getGeCampo14());
        objeto.setGeCampo15(        datos.getGeCampo15());
        objeto.setFultmod(          utilerias.calendarAFecha(utilerias.fechaActual()));
        objeto.setUultmod(          usuario);
        objeto.setPultmod(          "WSCA");

        // INVOCA LA ACCION EN EL DAO, DEVUELVE LO QUE DA EL DAO
        return dao.actualizar(objeto, usuario);
    }

    /**
     * Metodo para borrar un registro y sus dependencias
     * @param llave    La llave del registro
     * @param usuario  El usuario que ejecuta el metodo
     * @return Si se borra correctamente
     */
    public boolean borrar(final PKClienteObj llave, final String usuario){
        // INVOCA LA ACCION EN EL DAO, DEVUELVE LO QUE DA EL DAO
        return dao.borrar(llave, usuario) && dao.borrarDependencias(llave, usuario);
    }

    /**
     * Metodo para validar la existencia un registro
     * @param llave     La llave del registro
     * @param usuario   El usuario que ejecuta el metodo
     * @return Si existe o no
     */
    public boolean existe(final PKClienteObj llave, final String usuario){
        // INVOCA LA ACCION EN EL DAO, DEVUELVE LO QUE DA EL DAO
        return dao.existe(llave, usuario);
    }

    /**
     * Metodo para validar la existencia un registro con una cuenta dada
     * @param llave     La llave del registro
     * @param cuenta    La cuenta a verificar
     * @param usuario   El usuario que ejecuta el metodo
     * @return Si existe o no
     */
    public boolean existeCuenta(final PKClienteObj llave, final String cuenta, final String usuario){
        ClienteObj cliente;
        // SI EXISTE CON ESA LLAVE, VERIFICA LA CUENTA
        if(dao.existe(llave, usuario)){
            // UNA VEZ CARGADO, COMPARA LA CUENTA
            cliente = cargar(llave, usuario);
            // SI LA CUENTA ES IGUAL, DEVUELVE CORRECTO, EN OTRO CASO FALSO
            if(cuenta.compareTo(cliente.getNoCuenta()) == 0){
                return true;
            }else{
                return false;
            }
        // EN OTRO CASO, REGRESA FALSO
        }else{
            return false;
        }
    }

    /**
     * Metodo para agregar a la lista de clientes los registros del archivo
     * @param propietario   El propietario
     * @param clientes      La lista de clientes
     * @param usuario       El usuario que ejecuta el metodo
     */
    public void agregaClientes(final String propietario, final List<RegistroClienteObj> clientes, final String usuario) {
        ClienteObj          cliente     = null;
        PKClienteObj        llave      = null;

        // CONVIERTE LOS DATOS A WCOBRANZA OBJ
        for (RegistroClienteObj elemento: clientes) {
            llave = new PKClienteObj();
            llave.setAfiliacion(   propietario);
            llave.setCvReferencia( elemento.getReferencia());
            // SOLAMENTE SE INCORPORAN LOS REGISTROS QUE TIENEN DATOS CORRECTOS Y NO EXISTEN EN LA BD
            if(elemento.valido() && !dao.existe(llave, usuario)){
                cliente = new ClienteObj();
                cliente.setLlave(            llave);
                cliente.setNoCuenta(         elemento.getCuenta());
                cliente.setNmCliente(        encriptadorPropago.encripta(elemento.getNombre()));
                cliente.setNmPaterno(        encriptadorPropago.encripta(elemento.getPaterno()));
                cliente.setNmMaterno(        encriptadorPropago.encripta(elemento.getMaterno()));
                cliente.setMtDefault(        new Double(elemento.getImporte()));
                cliente.setFeIniContrato(    utilerias.cadenaAFecha2(elemento.getInicio()));
                cliente.setFeFinContrato(    utilerias.cadenaAFecha2(elemento.getFin()));
                cliente.setNumeroContrato(   elemento.getContrato());
                cliente.setCvActivo(         elemento.getActivo());
                cliente.setFultmod(          new Date());
                cliente.setPultmod(          "WSCA");
                cliente.setUultmod(          usuario);
                // INSERTA EL REGISTRO DE CLIENTE
                dao.crear(cliente, usuario);
            }
        }
    }

    /**
     * Metodo para cargar el contenido del archivo
     * @param propietario   El propietario
     * @param nombre        El archivo a procesar
     * @param nombrecorto   El archivo a procesar
     * @param usuario       El usuario que ejecuta el metodo
     */
    public void incorporaArchivoClientes(final String propietario, final String nombre, final String nombrecorto, final String usuario){
        List<String>                origen      = null;
        List<RegistroClienteObj>    detalles    = null;
        String                      extension   = nombrecorto.substring(nombrecorto.length() - 4).toLowerCase();

        // CARGA EL ARCHIVO USANDO STREAMS Y LAS FACILIDADES DE JAVA 8
        // CON  TRY-WITH-RESOURCES Y STREAM
        try (Stream<String> stream = Files.lines(Paths.get(nombre))) {
            // SOLAMENTE SE PERMITEN ARCHIVOS TXT Y EXCEL (XLS Y XLSX)
            if(".txt".compareTo(extension) == 0){
                origen = stream
                         .map(Object::toString)
                         .collect(Collectors.toList());
                origen.stream().forEach(System.out::println);

                // ARMA LA LISTA DE OBJETOS YA CON LA CADENA INICIALIZADA
                detalles = origen.stream()
                           .map(dato -> new RegistroClienteObj((String)dato))
                           .collect(Collectors.toList());

                // AGREGA LOS ELEMENTOS A LA LISTA DE COBRANZA
                agregaClientes(propietario, detalles, usuario);
            }else{
                // EL ARCHIVO SE PROCESA COMO EXCEL
                incorporaArchivoClientesExcel(propietario, nombre, nombrecorto, usuario);
            }
        } catch (IOException ex) {
            logger.logErrores(nombre, "Al cargar archivo de clientes", "BAN01", ex, "sys");
        }
    }

    /**
     * Metodo para cargar el contenido del archivo
     * @param propietario   El propietario
     * @param nombre        El archivo a procesar
     * @param nombrecorto   El archivo a procesar
     * @param usuario       El usuario que ejecuta el metodo
     */
    public void incorporaArchivoClientesExcel(final String propietario, final String nombre, final String nombrecorto, final String usuario){
        List<RegistroClienteObj>    detalles    = new ArrayList();
        RegistroClienteObj          registro    = null;

        Workbook workbook = null;
        int i;
        try {
            workbook = Workbook.getWorkbook(new File(nombre));

            // TOMA LA PRIMERA HOJA
            Sheet sheet = workbook.getSheet(0);
            i = 0;
            do{
                // LEE TODOS LOS REGISTROS QUE TIENEN DATOS, CUANDO YA NO HAY INFO GENERA EXCEPCION
                // ARMA EL REGISTRO CON EL CONTENIDO DE LAS DIEZ COLUMNAS
                registro = new RegistroClienteObj(sheet.getCell(0, i).getContents(), sheet.getCell(1, i).getContents(), sheet.getCell(2, i).getContents(),
                        sheet.getCell(3, i).getContents(), sheet.getCell(4, i).getContents(), sheet.getCell(5, i).getContents(),
                        sheet.getCell(6, i).getContents(), sheet.getCell(7, i).getContents(), sheet.getCell(8, i).getContents(),
                        sheet.getCell(9, i).getContents());
                detalles.add(registro);
                i++;
            }while(true);
        } catch (Exception ex) {
            logger.logErrores(nombre, "Al cargar archivo de clientes Excel", "BAN01", ex, "sys");
        } finally {
            if (workbook != null) {
                workbook.close();
            }
            // AGREGA LOS ELEMENTOS A LA LISTA DE COBRANZA
            agregaClientes(propietario, detalles, usuario);
        }
    }
}
