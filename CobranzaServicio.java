// ----------------------------------------------------------------------------
// Nombre del Programa : CobranzaServicio
// Autor               : Manuel Villalobos
// Compania            : GSOF
// Proyecto/ProGeneral : D-52-8272-17                    Fecha: 02/ABR/2018
// Descripcion General : CLASE SERVICIO PARA COBRANZA
// Programa Dependiente: N/A
// Programa Subsecuente: N/A
// Cond. de ejecucion  : N/A
// Dias de ejecucion   : N/A                                 Horario: N/A
//                              MODIFICACIONES
// ----------------------------------------------------------------------------
// Numero de Parametros:
// Parametros Entrada  :                                    Formato:
//
// Parametros Salida   : N/A                                Formato: N/A
// ----------------------------------------------------------------------------

package mx.com.prosa.wsca.servicio;

import mx.com.prosa.wsca.beans.*;
import mx.com.prosa.wsca.modelo.*;
import mx.com.prosa.wsca.util.*;
import mx.com.prosa.wsca.fabrica.*;
import java.util.Date;
import java.io.InputStream;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;
import java.util.stream.Collectors;
import java.io.File;
import java.io.IOException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

/**
 * Clase Servicio para Cobranza
 * @author Manuel Villalobos (GSOF)
 * @version 02/ABR/2018
 */
public class CobranzaServicio extends BaseServicio implements ICobranzaServicio {
    private static  CobranzaServicio         instancia              = null;
    private         ICobranzaDAO             daoCobranza            = CobranzaFabrica.getDAOCobranza();
    private         IKgcobroDAO              daoKgcobro             = CobranzaFabrica.getDAOKgcobro();
    private         IKdcobroDAO              daoKdcobro             = CobranzaFabrica.getDAOKdcobro();
    private         IRecepcionDAO            daoRecepcion           = RecepcionFabrica.getDAO();
    private         ClienteServicio          servicioCliente        = ClienteFabrica.getServicio();
    private         AfiliacionServicio       servicioAfiliacion     = AfiliacionFabrica.getServicio();
    private         BancoServicio            servicioBanco          = BancoFabrica.getServicio();
    private         ConfiguracionServicio    servicioConfiguracion  = ConfiguracionFabrica.getServicio();
    private         EncriptadorPropago       encriptadorPropago     = AyudasFabrica.getEncriptadorPropago();
    private         ParametroServicio        servicioParametros     = ParametroFabrica.getServicio();
    private         String                   rutaOut                = servicioParametros.elemento("WORKSPACE_OUT", "sys").getValor();

    /**
     * Constructor privado
     */
    private CobranzaServicio() {
        setNombre("Cobranzas");
    }

    /**
     * Metodo para obtener la instancia del Singleton
     * @return La instancia del singleton
     */
    public static CobranzaServicio getInstancia() {
        if(instancia == null) {
            instancia = new CobranzaServicio();
        }

        return instancia;
    }

    /**
     * Metodo para depurar la lista de cobranza de un propietario
     * @param propietario   El propietario
     * @param usuario       El usuario que ejecuta el metodo
     * @return Si fue depurada o no
     */
    public boolean depurarWCobranza(final String propietario, final String usuario) {
        return daoCobranza.depurarWCobranza(propietario, usuario);
    }

    /**
     * Metodo para depurar la lista de cobranza completa
     * @param usuario       El usuario que ejecuta el metodo
     * @return Si fue depurada o no
     */
    public boolean depurarWCobranza(final String usuario) {
        return daoCobranza.depurarWCobranza(usuario);
    }

    /**
     * Metodo para obtener un listado de las listas de cobranza
     * @param propietario   El propietario
     * @param afiliacion    La afiliacion a la que se le esta haciendo el archivo
     * @param feCorte       La fecha de corte indicada
     * @param hrCorte       La hora de corte indicada
     * @param feProgramada  La fecha programada indicada
     * @param usuario       El usuario que ejecuta el metodo
     * @return El resultado
     */
    public ArchivoCobranza IniciaListaCobranza(final String propietario, final String afiliacion, final String feCorte, final String hrCorte, final String feProgramada, final String usuario){
        ArchivoCobranza archivo = new ArchivoCobranza();

        // OBTIENE EL FOLIO
        String folio = servicioConfiguracion.cargar(propietario, usuario).getFolio();
        folio = String.valueOf(Integer.parseInt(folio));
        archivo.setFolio(       folio);
        archivo.setAfiliacion(  afiliacion);
        archivo.setFeCorte(     feCorte);
        archivo.setHrCorte(     hrCorte);
        archivo.setBanco(       listasDAO.bancoAfiliacion(propietario, afiliacion, usuario));
        archivo.setMinimo(      listasDAO.montoBancoAfiliacion(propietario, afiliacion, usuario));
        archivo.setOrden(       null);

        // SI NO HAY DATOS DE PROGRAMADA, PONE LA FECHA DE CORTE
        if((feProgramada == null) || (feProgramada.length() == 0)){
            archivo.setFeProgramada(feCorte + " " + hrCorte);
            archivo.setIndProgramada(0);
        // EN OTRO CASO PONE LA FECHA PROGRAMADA
        }else{
            archivo.setFeProgramada(feProgramada);
            archivo.setIndProgramada(1);
        }

        // REGRESA EL ARCHIVO CONFIGURADO
        return archivo;
    }

    /**
     * Metodo para obtener un listado de las listas de cobranza
     * @param propietario   El propietario
     * @param folio         El folio
     * @param usuario       El usuario que ejecuta el metodo
     * @return El resultado
     */
    public boolean borrarLista(final String propietario, final String folio, final String usuario){
        daoKgcobro.depurarKgcobro(  propietario, folio, usuario);
        daoKdcobro.depurarKdcobro(  propietario, folio, usuario);
        daoCobranza.depurarArchivo( propietario, folio, usuario);
        return true;
    }

    /**
     * Metodo para obtener un listado de las listas de cobranza almacenadas
     * @param propietario   El propietario
     * @param usuario       El usuario que ejecuta el metodo
     * @return La lista
     */
    public List<KgcobroObj> obtenerListas(final String propietario, final String usuario){
        List regreso = daoKgcobro.listar(propietario);
        return regreso;
    }

    /**
     * Metodo para obtener el total de elementos que cumplen con los criterios
     * @param afiliacion    La afiliacion propietaria
     * @param usuario       Filtro usuario
     * @return Lista de elementos de tipo EventoObj
     */
    public int totalesKg(final String afiliacion, final String usuario){
        // INVOCA LA ACCION EN EL DAO, DEVUELVE LO QUE DA EL DAO
        return daoKgcobro.totales(afiliacion, usuario);
    }

    /**
     * Metodo para obtener el contenido del catalogo, paginado
     * @param afiliacion    La afiliacion propietaria
     * @param usuario       Filtro usuario
     * @param pagina        El numero de paginas
     * @param bloque        El tamanio el bloque
     * @return Lista de elementos de tipo EventoObj
     */
    public List detallesKg(final String afiliacion, final String usuario, final int pagina, final int bloque){
        return daoKgcobro.detalles(afiliacion, usuario, pagina, bloque);
    }

    /**
     * Metodo para poner a wcobranza los detalles de un folio
     * @param propietario   El propietario
     * @param folio         El folio
     * @param usuario       El usuario que ejecuta el metodo
     */
    public void cargaDetalleCobranzaAlmacenadaKdcobro(final String propietario, final String folio, final String usuario){
        // OBTIENE LA LISTA DE DETALLES DE KDCOBRO
        List<KdcobroObj>    lista       = daoKdcobro.listar(propietario, folio, usuario);
        ClienteObj          cliente     = null;
        PKClienteObj        llave       = null;
        WCobranzaObj        wc          = null;

        // DEPURA WCOBRANZA
        daoCobranza.depurarWCobranza(propietario, usuario);

        // CONVIERTE LOS DATOS A WCOBRANZA OBJ
        for (KdcobroObj elemento: lista) {
            // OBTIENE EL DETALLE DEL CLIENTE
            llave = new PKClienteObj();
            llave.setAfiliacion(propietario);
            llave.setCvReferencia(elemento.getCvReferencia());

            // EL CLIENTE YA VIENE DESENCRIPTADO
            cliente = servicioCliente.cargar(llave, usuario);

            wc = new WCobranzaObj();
            wc.setAfiliacion(      propietario);
            wc.setCvAfiliacion(    elemento.getCvAfiliacion());
            wc.setCvReferencia(    elemento.getCvReferencia());
            wc.setNoCuenta(        elemento.getNoCuenta());
            wc.setNmCliente(       encriptadorPropago.encripta(cliente.getNmCliente()));
            wc.setNmPaterno(       encriptadorPropago.encripta(cliente.getNmPaterno()));
            wc.setNmMaterno(       encriptadorPropago.encripta(cliente.getNmMaterno()));
            wc.setFeVigencia(      encriptadorPropago.encripta(cliente.getFeVigencia()));
            wc.setCvMovimiento(    elemento.getCvMovimiento());
            wc.setMtImporte(       elemento.getMtImporte());
            wc.setFeProgramada(    elemento.getFeProgramada());
            wc.setFeCorte(         elemento.getFeCorte());
            wc.setNumeroContrato(  elemento.getNumeroContrato());

            // INSERTA LOS REGISTROS DE LA LISTA DE COBRANZA
            daoCobranza.insertaWc(wc, usuario);
        }
    }

    /**
     * Metodo para obtener el detalle de la cobranza
     * @param propietario   El propietario
     * @param orden         El orden a aplicar
     * @param usuario       El usuario que ejecuta el metodo
     * @return La lista
     */
    public List<WCobranzaObj> cargaDetalleCobranza(final String propietario, final String orden, final String usuario){
        List<WCobranzaObj>  listawc     = daoCobranza.listarWCobranza(propietario, orden, usuario);
        ClienteObj          cliente     = null;
        WCobranzaObj        wc          = null;

        // DESENCRIPTA Y ENMASCARA
        for (WCobranzaObj elemento: listawc) {
            elemento.setNoCuenta(         utilerias.enmascararTarjeta(elemento.getNoCuenta()));
            elemento.setNmCliente(        encriptadorPropago.desencripta(elemento.getNmCliente()));
            elemento.setNmPaterno(        encriptadorPropago.desencripta(elemento.getNmPaterno()));
            elemento.setNmMaterno(        encriptadorPropago.desencripta(elemento.getNmMaterno()));
        }

        return listawc;
    }

    /**
     * Metodo para calcular los detalles de una lista de cobranza
     * @param propietario   El propietario
     * @param lista         La lista de cobranza
     * @param minimo        El monto minimo
     * @param usuario       El usuario que ejecuta el metodo
     * @return El archivo con los totales calculados
     */
    public ArchivoCobranza calculaTotalesListaCobranza(final String propietario, final List<WCobranzaObj> lista, final Double minimo, final String usuario){
        ArchivoCobranza archivo     = new ArchivoCobranza();
        Long            invalidos   = 0L;
        Long            registros   = new Long(lista.size());
        Double          monto       = 0.0;

        // SACA LOS TOTALES A PARTIR DE LOS DETALLES
        for (WCobranzaObj elemento: lista) {
            monto += elemento.getMtImporte();
            if(elemento.getMtImporte() < minimo){
                invalidos++;
            }
        }
        archivo.setRegistros(   registros);
        archivo.setMonto(       monto);
        archivo.setInvalidos(   invalidos);

        return archivo;
    }

    /**
     * Metodo para obtener un listado de las listas de cobranza anteriores a una fecha y hora y eliminarlos
     * @param propietario   El propietario
     * @param fecha         La fecha
     * @param hora          La hora
     * @param usuario       El usuario que ejecuta el metodo
     */
    public int borrarListasAnterioresFecha(final String propietario, final String fecha, final String hora, final String usuario){
        // OBTIENE LOS FOLIOS A BORRAR
        List<EstructuraLista> folios = daoKgcobro.listarAnteriores(propietario, fecha, hora, usuario);

        // RECORRE LA LISTA Y POR CADA FOLIO LO BORRA DE KG, KD Y ARCHIVOS
        for (EstructuraLista elemento: folios) {
            daoKgcobro.depurarKgcobro(  propietario, elemento.getClave(), usuario);
            daoKdcobro.depurarKdcobro(  propietario, elemento.getClave(), usuario);
            daoCobranza.depurarArchivo( propietario, elemento.getClave(), usuario);
        }
        return folios.size();
    }

    /**
     * Metodo para obtener la lista de clientes activos y con contrato vigente
     * @param propietario   La afiliacion propietaria
     * @param cvafiliacion  La afiliacion de la que se quieren los clientes
     * @param usuario       El usuario que ejecuta el metodo
     * @return La lista de elementos
     */
    public int totalesEspecificos(final String propietario, final String cvafiliacion, final String usuario) {
        return daoCobranza.totalesEspecificos(propietario, cvafiliacion, usuario);
    }

    /**
     * Metodo para obtener el contenido del catalogo, paginado
     * @param propietario   La afiliacion propietaria
     * @param cvafiliacion  La afiliacion de la que se quieren los clientes
     * @param usuario       Filtro usuario
     * @param pagina        El numero de paginas
     * @param bloque        El tamanio el bloque
     * @return Lista de elementos de tipo EventoObj
     */
    public List detallesEspecificos(final String propietario, final String cvafiliacion, final String usuario, final int pagina, final int bloque) {
        // OBTIENE LA LISTA DE CLIENTES
        List<ClienteObj> clientes = new ArrayList();
        List<ClienteObj> lista = daoCobranza.detallesEspecificos(propietario, cvafiliacion, usuario, pagina, bloque);

        for (ClienteObj cliente : lista){
            cliente.setNmCliente(        encriptadorPropago.desencripta(cliente.getNmCliente()));
            cliente.setNmPaterno(        encriptadorPropago.desencripta(cliente.getNmPaterno()));
            cliente.setNmMaterno(        encriptadorPropago.desencripta(cliente.getNmMaterno()));
            cliente.setNoCuenta(         utilerias.enmascararTarjeta((cliente.getNoCuenta())));
            clientes.add(cliente);
        }

        return clientes;
    }

    /**
     * Metodo para obtener la lista de clientes activos y con contrato vigente
     * @param propietario   La afiliacion propietaria
     * @param cvafiliacion  La afiliacion de la que se quieren los clientes
     * @param usuario       El usuario que ejecuta el metodo
     * @return La lista de elementos
     */
    public List<ClienteObj> obtenerClientesEspecificos(final String propietario, final String cvafiliacion, final String usuario) {
        // OBTIENE LA LISTA DE CLIENTES
        List<ClienteObj> clientes = new ArrayList();
        List<ClienteObj> lista = daoCobranza.obtenerClientes(propietario, cvafiliacion, usuario);

        for (ClienteObj cliente : lista){
            cliente.setNmCliente(        encriptadorPropago.desencripta(cliente.getNmCliente()));
            cliente.setNmPaterno(        encriptadorPropago.desencripta(cliente.getNmPaterno()));
            cliente.setNmMaterno(        encriptadorPropago.desencripta(cliente.getNmMaterno()));
            cliente.setNoCuenta(         utilerias.enmascararTarjeta((cliente.getNmMaterno())));
            clientes.add(cliente);
        }

        return clientes;
    }

    /**
     * Metodo para obtener la lista de clientes activos y con contrato vigente, de rechazos
     * @param propietario   La afiliacion propietaria
     * @param cvafiliacion  La afiliacion de la que se quieren los clientes
     * @param motivo        El motivo
     * @param usuario       El usuario que ejecuta el metodo
     * @return La lista de elementos
     */
    public int totalesRechazos(final String propietario, final String cvafiliacion, final String motivo, final String usuario) {
        return daoCobranza.totalesRechazos(propietario, cvafiliacion, motivo, usuario);
    }

    /**
     * Metodo para obtener el contenido de clienets de rechazos
     * @param propietario   La afiliacion propietaria
     * @param cvafiliacion  La afiliacion de la que se quieren los clientes
     * @param motivo        El motivo
     * @param usuario       Filtro usuario
     * @param pagina        El numero de paginas
     * @param bloque        El tamanio el bloque
     * @return Lista de elementos de tipo EventoObj
     */
    public List detallesRechazos(final String propietario, final String cvafiliacion, final String motivo, final String usuario, final int pagina, final int bloque) {
        // OBTIENE LA LISTA DE CLIENTES
        List<ClienteObj> clientes = new ArrayList();
        List<KdcobroObj> lista = daoCobranza.detallesRechazos(propietario, cvafiliacion, motivo, usuario, pagina, bloque);
        KdcobroObj rechazo = null;
        PKClienteObj llave = null;
        ClienteObj cliente = null;

        for (KdcobroObj detalle : lista){
            llave = new PKClienteObj();
            llave.setAfiliacion(        propietario);
            llave.setCvReferencia(      detalle.getCvReferencia());
            cliente = servicioCliente.cargar(llave, usuario);
            cliente.setNoCuenta(         utilerias.enmascararTarjeta((cliente.getNoCuenta())));
            cliente.setMtDefault(        detalle.getMtImporte());
            cliente.setGeCampo1(         detalle.getCvMovimiento());
            clientes.add(cliente);
        }

        return clientes;
    }

    /**
     * Metodo para obtener la lista de clientes activos y con contrato vigente de rechazos
     * @param propietario   La afiliacion propietaria
     * @param cvafiliacion  La afiliacion a la que se le esta armando la lista
     * @param fecha         La fecha criterio
     * @param motivo        El motivo
     * @param usuario       El usuario que ejecuta el metodo
     * @return La lista de elementos
     */
    public List<ClienteObj> obtenerClientesRechazos(final String propietario, final String cvafiliacion, final String fecha, final String motivo, final String usuario) {
        // OBTIENE LA LISTA DE CLIENTES
        List<ClienteObj> clientes = new ArrayList();
        ClienteObj cliente = null;
        PKClienteObj llave = null;
        List<KdcobroObj> rechazos = daoCobranza.obtenerRechazos(propietario, cvafiliacion, motivo, usuario);

        // A LOS RECHAZOS OBTENIDOS SE LES EXTRAE LA INFORMACION Y SE COMPLEMENTA CON LOS CLIENTES
        for (KdcobroObj elemento: rechazos) {
            // OBTIENE EL CLIENTE
            llave = new PKClienteObj();
            llave.setAfiliacion(    propietario);
            llave.setCvReferencia(  elemento.getCvReferencia());
            cliente = servicioCliente.cargar(llave, usuario);
            cliente.setMtDefault(   elemento.getMtImporte());
            cliente.setNmCliente(   encriptadorPropago.desencripta(cliente.getNmCliente()));
            cliente.setNmPaterno(   encriptadorPropago.desencripta(cliente.getNmPaterno()));
            cliente.setNmMaterno(   encriptadorPropago.desencripta(cliente.getNmMaterno()));
            cliente.setNoCuenta(    utilerias.enmascararTarjeta((cliente.getNmMaterno())));
            cliente.setGeCampo1(    elemento.getCvMovimiento());
            clientes.add(cliente);
        }
        return clientes;
    }

    /**
     * Metodo para insertar una lista de cvreferencia de cliente a la lista de cobranza
     * @param propietario   La afiliacion propietaria
     * @param referencias   La lista de clientes elegidos
     * @param cvafiliacion  La afiliacion a la que se le esta armando la lista
     * @param feprogramada  La fecha programada
     * @param fecorte       La fecha de corte
     * @param usuario       El usuario que ejecuta el metodo
     */
    public void agregaClientesElegidosCobranza(final String propietario, final String[] referencias, final String cvafiliacion, final String feprogramada, final String fecorte, final String usuario) {
        ClienteObj          cliente     = null;
        PKClienteObj        llave       = null;
        WCobranzaObj        wc          = null;

        // CONVIERTE LOS DATOS A WCOBRANZA OBJ
        for (String elemento: referencias) {
            // DESCARTA LOS ELEMENTOS QUE SOLAMENTE TRAEN BLANCOS
            if(elemento.length() >0){
                // OBTIENE EL DETALLE DEL CLIENTE
                llave = new PKClienteObj();
                llave.setAfiliacion(propietario);
                llave.setCvReferencia(elemento);

                // EL CLIENTE YA VIENE DESENCRIPTADO
                cliente = servicioCliente.cargar(llave, usuario);
                wc = new WCobranzaObj();
                wc.setAfiliacion(       propietario);
                wc.setCvAfiliacion(     cvafiliacion);
                wc.setCvReferencia(     elemento);
                wc.setNoCuenta(         cliente.getNoCuenta());
                wc.setNmCliente(        encriptadorPropago.encripta(cliente.getNmCliente()));
                wc.setNmPaterno(        encriptadorPropago.encripta(cliente.getNmPaterno()));
                wc.setNmMaterno(        encriptadorPropago.encripta(cliente.getNmMaterno()));
                wc.setCvMovimiento(     "00");
                wc.setMtImporte(        cliente.getMtDefault());
                if(feprogramada == null){
                    wc.setFeProgramada(     new Date());
                }else{
                    wc.setFeProgramada(     utilerias.cadenaAFecha(feprogramada));
                }
                if(feprogramada == null){
                    wc.setFeCorte(          new Date());
                }else{
                    wc.setFeCorte(          utilerias.cadenaAFecha(fecorte));
                }
                wc.setNumeroContrato(   cliente.getNumeroContrato());

                // INSERTA LOS REGISTROS DE LA LISTA DE COBRANZA
                daoCobranza.insertaWc(wc, usuario);
            }
        }
    }

    /**
     * Metodo para acumular los registros en cobranza
     * @param propietario   La afiliacion propietaria
     * @param usuario       El usuario que ejecuta el metodo
     */
    public void acumulaCobranza(final String propietario, final String usuario) {
        // OBTIENE LOS ACUMULADOS
        List<WCobranzaObj> lista = daoCobranza.listarWCobranzaAcumulada(propietario, usuario);

        // DEPURA WCOBRANZA
        daoCobranza.depurarWCobranza(propietario, usuario);

        // INSERTA LOS DATOS ACUMULADOS
        for (WCobranzaObj elemento: lista) {
            // INSERTA LOS REGISTROS DE LA LISTA DE COBRANZA
            elemento.setAfiliacion(propietario);
            daoCobranza.insertaWc(elemento, usuario);
        }
    }

    /**
     * Metodo para actualizar el monto de los registros en cobranza
     * @param propietario   La afiliacion propietaria
     * @param usuario       El usuario que ejecuta el metodo
     */
    public void actualizaCobranza(final String propietario, final String monto, final String usuario) {
        // SE ACTUALIZA EL MONTO SOLAMENTE SI ES UN DECIMAL
        if(validaciones.esDecimalPositivo(monto)){
            // ACTUALIZA EL NUEVO MONTO EN WCOBRANZA
            daoCobranza.actualizarWCobranza(propietario, monto, usuario);
        }
    }

    /**
     * Metodo para depurar todos los registros en cobranza
     * @param usuario       El usuario que ejecuta el metodo
     */
    public void depurarCobranza(final String usuario) {
        // DEPURA WCOBRANZA
        daoCobranza.depurarWCobranza(usuario);
    }

    /**
     * Metodo para depurar la lista de cobranza de un propietario
     * @param propietario   El propietario
     * @param indice        El indice del registro
     * @param usuario       El usuario que ejecuta el metodo
     */
    public void borrarRegistroWCobranza(final String propietario, final String indice, final String usuario) {
        // BORRA EL REGISTRO
        daoCobranza.borrarRegistroWCobranza(propietario, indice, usuario);
    }

    /**
     * Metodo para agregar a la lista de cobranza los cobros de un archivo incorporado
     * @param propietario   El propietario
     * @param cobros        La lista de cobros
     * @param usuario       El usuario que ejecuta el metodo
     */
    public void agregaCobrosArchivo(final String propietario, final List<RegistroCobroObj> cobros, final String usuario) {
        ClienteObj          cliente     = null;
        PKClienteObj        llave1      = null;
        PKAfiliacionObj     llave2      = null;
        WCobranzaObj        wc          = null;

        // CONVIERTE LOS DATOS A WCOBRANZA OBJ
        for (RegistroCobroObj elemento: cobros) {
            llave1 = new PKClienteObj();
            llave1.setAfiliacion(   propietario);
            llave1.setCvReferencia( elemento.getReferencia());

            llave2 = new PKAfiliacionObj();
            llave2.setAfiliacion(   propietario);
            llave2.setCvAfiliacion( elemento.getAfiliacion());

            // SOLAMENTE SE INCORPORAN LOS REGISTROS QUE TIENEN DATOS CORRECTOS
            if(elemento.valido() && servicioAfiliacion.existe(llave2, usuario)
                && servicioCliente.existe(llave1, usuario) && servicioCliente.existeCuenta(llave1, elemento.getCuenta(), usuario)){
            // SOLAMENTE SE INCORPORAN LOS REGISTROS QUE TIENEN DATOS CORRECTOS
                // OBTIENE EL DETALLE DEL CLIENTE
                llave1 = new PKClienteObj();
                llave1.setAfiliacion(propietario);
                llave1.setCvReferencia(elemento.getReferencia());

                // EL CLIENTE YA VIENE DESENCRIPTADO
                cliente = servicioCliente.cargar(llave1, usuario);
                wc = new WCobranzaObj();
                wc.setAfiliacion(       propietario);
                wc.setCvAfiliacion(     elemento.getAfiliacion());
                wc.setCvReferencia(     elemento.getReferencia());
                wc.setNoCuenta(         elemento.getCuenta());
                wc.setNmCliente(        encriptadorPropago.encripta(cliente.getNmCliente()));
                wc.setNmPaterno(        encriptadorPropago.encripta(cliente.getNmPaterno()));
                wc.setNmMaterno(        encriptadorPropago.encripta(cliente.getNmMaterno()));
                wc.setFeVigencia(       "XXXX");
                wc.setCvMovimiento(     elemento.getMovimiento());
                wc.setMtImporte(        new Double(elemento.getImporte()));
                wc.setFeProgramada(     new Date());
                wc.setFeCorte(          new Date());
                wc.setNumeroContrato(   cliente.getNumeroContrato());

                // INSERTA EL REGISTROS DE COBRANZA
                daoCobranza.insertaWc(wc, usuario);
            }else{
            }
        }
    }

    /**
     * Metodo para obtener el contenido del catalogo
     * @param propietario   El propietario
     * @param nombre        El archivo a procesar
     * @param nombrecorto   El archivo a procesar
     * @param usuario       El usuario que ejecuta el metodo
     */
    public void incorporaArchivoCobros(final String propietario, final String nombre, final String nombrecorto, final String usuario){
        List<String>            origen      = null;
        List<RegistroCobroObj>  detalles    = null;

        // CARGA EL ARCHIVO USANDO STREAMS Y LAS FACILIDADES DE JAVA 8
        // CON  TRY-WITH-RESOURCES Y STREAM
        try (Stream<String> stream = Files.lines(Paths.get(nombre))) {
            origen = stream
                     .map(Object::toString)
                     .collect(Collectors.toList());

            // ARMA LA LISTA DE OBJETOS YA CON LA CADENA INICIALIZADA
            detalles = origen.stream()
                       .map(dato -> new RegistroCobroObj((String)dato))
                       .collect(Collectors.toList());

            // AGREGA LOS ELEMENTOS A LA LISTA DE COBRANZA
            agregaCobrosArchivo(propietario, detalles, usuario);

        } catch (IOException ex) {
            logger.logErrores(nombre, "Al cargar archivo de cobros", "BAN01", ex, "sys");
        }
    }

    /**
     * Metodo para obtener la lista de afiliaciones, con opcion Todas al inicio
     * @param propietario   El comercio propietario
     * @param usuario       El usuario que ejecuta el metodo
     * @return La Lista
     */
    public List listaAfiliacionesTodas(final String propietario, final String usuario) {
        return listasDAO.listaAfiliacionesTodas(propietario, usuario);
    }

    /**
     * Metodo para obtener la lista de afiliaciones
     * @param propietario   El comercio propietario
     * @param usuario       El usuario que ejecuta el metodo
     * @return La Lista
     */
    public List listaAfiliaciones(final String propietario, final String usuario) {
        return listasDAO.listaAfiliaciones(propietario, usuario);
    }

    /**
     * Metodo para obtener la lista de motivos de rechazo, con opcion Todos al inicio
     * @param usuario       El usuario que ejecuta el metodo
     * @return La Lista
     */
    public List listaMotivosTodos(final String usuario) {
        return listasDAO.listaMotivosTodos(usuario);
    }

    /**
     * Metodo para obtener los totales de la lista de wcobranza
     * @param propietario   La afiliacion propietaria
     * @param orden         El orden
     * @param usuario       El usuario que ejecuta el metodo
     * @return La lista de elementos
     */
    public int totalesCobranza(final String propietario, final String orden, final String usuario){
        return daoCobranza.totalesCobranza(propietario, orden, usuario);
    }

    /**
     * Metodo para obtener el contenido de wcobranza, paginado
     * @param propietario   La afiliacion propietaria
     * @param orden         El orden
     * @param usuario       Filtro usuario
     * @param pagina        El numero de paginas
     * @param bloque        El tamanio el bloque
     * @return Lista de elementos de tipo EventoObj
     */
    public List detallesCobranza(final String propietario, final String orden, final String usuario, final int pagina, final int bloque){
        List<WCobranzaObj> lista = daoCobranza.detallesCobranza(propietario, orden, usuario, pagina, bloque);
            for(WCobranzaObj wc: lista) {
                wc.setNmCliente(        encriptadorPropago.desencripta(wc.getNmCliente()));
                wc.setNmPaterno(        encriptadorPropago.desencripta(wc.getNmPaterno()));
                wc.setNmMaterno(        encriptadorPropago.desencripta(wc.getNmMaterno()));
                wc.setNoCuenta(         utilerias.enmascararTarjeta((wc.getNoCuenta())));
            }
        return lista;
    }

    /**
     * Metodo para hacer el guardado de la lista de cobranza
     * @param propietario   La afiliacion propietaria
     * @param archivo       El archivo de datos
     * @param usuario       El usuario que ejecuta el metodo
     */
    public void guardaLista(final String propietario, final ArchivoCobranza archivo, final String usuario) {
        KgcobroObj          kg        = null;
        KdcobroObj          kd        = null;
        ConfiguracionObj    configura = null;
        PKKgcobroObj        llave     = new PKKgcobroObj();
        PKKdcobroObj        llave2    = null;
        List<WCobranzaObj>  listawc   = null;
        List<KdcobroObj>    listakd   = null;
        int                 i         = 0;

        llave.setAfiliacion(propietario);
        llave.setFlg(new Integer(archivo.getFolio()));

        // VERIFICA SI EXISTE YA EN KG
        if(existeKg(propietario, archivo.getFolio(), usuario)){
            // CARGA EL REGISTRO DE KGCOBRO PARA HACER LA ACTUALIZACION
            kg = (KgcobroObj)daoKgcobro.cargar(llave, usuario);

            // PONE LOS DATOS DEL ARCHIVO EN KG Y LO GUARDA
            kg.setCvAfiliacion( archivo.getAfiliacion());
            kg.setFeCorte(      utilerias.cadenaAFecha3(archivo.getFeCorte() + " " + archivo.getHrCorte()));
            kg.setNmTransac(    archivo.getRegistros().intValue());
            kg.setGeImporte(    archivo.getMonto());
            kg.setCvBanco(      archivo.getBanco());
            kg.setUultmod(      usuario);
            kg.setFultmod(      new Date());
            daoKgcobro.actualizar(kg, usuario);

            // DEPURA LOS REGISTROS DE KDCOBRO
            daoKdcobro.depurarKdcobro(propietario, archivo.getFolio(), usuario);
        // EN OTRO CASO, LA LISTA ES NUEVA Y SE INSERTA EN KG Y KD
        }else{
            // SE ACTUALIZA EL FOLIO SOLAMENTE SI NO EXISTE
            servicioConfiguracion.actualizaFolio(propietario, usuario);

            kg = new KgcobroObj();
            kg.setLlave(        llave);
            kg.setCvAfiliacion( archivo.getAfiliacion());
            kg.setFeCorte(      utilerias.cadenaAFecha3(archivo.getFeCorte() + " " + archivo.getHrCorte()));
            kg.setNmTransac(    archivo.getRegistros().intValue());
            kg.setGeImporte(    archivo.getMonto());
            kg.setCvBanco(      archivo.getBanco());
            kg.setUultmod(      usuario);
            kg.setFultmod(      new Date());
            daoKgcobro.crear(kg, usuario);
        }
System.out.println("Corte:" + archivo.getFeCorte() + " " + archivo.getHrCorte());

System.out.println("Listando WCobranza");
        // EN CUALQUIER CASO SE INSERTA LA LISTA DE DETALLES A KDCOBRO, SIN ORDEN
        listawc = daoCobranza.listarWCobranza(propietario, null, usuario);
System.out.println("Lista:" + listawc.size());

        // LA CONVIERTE A KDCOBRO Y LA INSERTA
        for (WCobranzaObj elemento: listawc) {
            llave2 = new PKKdcobroObj();
            llave2.setAfiliacion(propietario);
            llave2.setFlg(new Integer(archivo.getFolio()));
            llave2.setFld(i++);
            kd = new KdcobroObj();
            kd.setLlave(        llave2);
            kd.setCvAfiliacion( elemento.getCvAfiliacion());
System.out.println("Cuenta:" + elemento.getNoCuenta());
            kd.setNoCuenta(     elemento.getNoCuenta());
            kd.setCvReferencia( elemento.getCvReferencia());
            kd.setCvMovimiento( elemento.getCvMovimiento());
System.out.println("Importe:" + elemento.getMtImporte());
            kd.setMtImporte(    elemento.getMtImporte());
System.out.println("Contrato:" + elemento.getNumeroContrato());
            kd.setNumeroContrato( elemento.getNumeroContrato());
System.out.println("FeCorte:" + utilerias.cadenaAFecha3(archivo.getFeCorte() + " " + archivo.getHrCorte()));
            kd.setFeCorte(      utilerias.cadenaAFecha3(archivo.getFeCorte() + " " + archivo.getHrCorte()));
System.out.println("FeProgramada:" + archivo.getFeProgramada());
            kd.setFeProgramada( utilerias.cadenaAFecha3(archivo.getFeProgramada() + " " + archivo.getHrCorte()));
            kd.setUultmod(      usuario);
            kd.setFultmod(      new Date());
            daoKdcobro.crear(kd, usuario);
        }
    }

    /**
     * Metodo para validar la existencia un registro de Kgcobro
     * @param propietario   La afiliacion propietaria
     * @param folio         El folio de la lista
     * @param usuario       El usuario que ejecuta el metodo
     * @return Si existe o no
     */
    public boolean existeKg(final String propietario, final String folio, final String usuario) {
        PKKgcobroObj llave = new PKKgcobroObj();
        llave.setAfiliacion(propietario);
        llave.setFlg(new Integer(folio));
        return daoKgcobro.existe(llave, usuario);
    }

    /**
     * Metodo para armar el archivo de cobranza a partir de la informacion en kgcobro
     * @param propietario   La afiliacion propietaria
     * @param folio         El folio de la lista
     * @param usuario       El usuario que ejecuta el metodo
     * @return Si existe o no
     */
    public ArchivoCobranza armaArchivoCobranza(final String propietario, final String folio, final String usuario) {
        ArchivoCobranza archivo = new ArchivoCobranza();
        PKKgcobroObj    llave   = new PKKgcobroObj();
        KgcobroObj      kg      = null;
        llave.setAfiliacion(propietario);
        llave.setFlg(new Integer(folio));
        kg      = (KgcobroObj)daoKgcobro.cargar(llave, usuario);
        archivo.setAfiliacion(  kg.getCvAfiliacion());
        archivo.setFolio(       folio);
        archivo.setRegistros(   new Long(kg.getNmTransac().toString()));
        archivo.setMonto(       kg.getGeImporte());
        archivo.setFeCorte(     utilerias.fechaFormatoddMMyyyy(new java.sql.Date(kg.getFeCorte().getTime())));
        archivo.setFeProgramada(utilerias.fechaFormatoddMMyyyy(new java.sql.Date(kg.getFeCorte().getTime())));
        archivo.setHrCorte(     utilerias.fechaFormatoHHMM(new java.sql.Date(kg.getFeCorte().getTime())));
        archivo.setBanco(       listasDAO.bancoAfiliacion(propietario, kg.getCvAfiliacion(), usuario));
        archivo.setMinimo(      listasDAO.montoBancoAfiliacion(propietario, kg.getCvAfiliacion(), usuario));
        archivo.setOrden(       null);

        return archivo;
    }

    /**
     * Metodo para verificar que un archivo existe y sin enviar
     * @param llave         La llave a buscar
     * @param usuario       El usuario que ejecuta el metodo
     * @return Si existe o no
     */
    public boolean existeArchivoSinEnviar(final PKArchivoObj llave, final String usuario) {
        return daoRecepcion.existeSinEnviar(llave, usuario);
    }

    /**
     * Metodo para verificar que un archivo existe
     * @param llave         La llave a buscar
     * @param usuario       El usuario que ejecuta el metodo
     * @return Si existe o no
     */
    public boolean existeArchivo(final PKArchivoObj llave, final String usuario) {
        return daoRecepcion.existe(llave, usuario);
    }

    /**
     * Metodo para hacer el archivo de la lista de cobranza
     * @param propietario   La afiliacion propietaria
     * @param archivo       El archivo de datos
     * @param usuario       El usuario que ejecuta el metodo
     * @return Nombre del archivo generado
     */
    public String generarArchivo(final String propietario, final ArchivoCobranza archivo, final String usuario) {
System.out.println("GeneraArchivo:" + propietario);
        PKArchivoObj llave = new PKArchivoObj();
        String nombre;
        llave.setAfiliacion(propietario);
        llave.setCvLista(new Integer(archivo.getFolio()));
        ArchivoObj arch = null;
        ConfiguracionObj config = (ConfiguracionObj)servicioConfiguracion.cargar(propietario, usuario);
System.out.println("Config:" + config.getIdentificador());

        // VERIFICA SI EXISTE EL ARCHIVO, SI EXISTE, OBTIENE EL NOMBRE DEL ARCHIVO
        if(existeArchivo(llave, usuario)){
            arch = (ArchivoObj)daoRecepcion.cargar(llave, usuario);

            // ACTUALIZA LOS DATOS
            arch.setStatus("00");
            arch.setFultmod(new Date());
            arch.setUultmod(usuario);
            daoRecepcion.actualizar(arch, usuario);
        // EN OTRO CASO LO GENERA
        }else{
            arch = new ArchivoObj();
System.out.println("Folio:" + new Integer(config.getFolio()));
System.out.println("Fecha:" + utilerias.fechaFormatoyyMMdd(new Date()));
            nombre = "SCAWEB" + config.getIdentificador() + "D" +
                        utilerias.fechaFormatoyyMMdd(new Date()) + "WR" +
                        utilerias.entero00.format(new Integer(config.getFolio())) + ".ftp";
System.out.println("Nombre:" + nombre);
            arch.setLlave(llave);
            arch.setStatus("00");
            arch.setNmArchivo(nombre);
            arch.setRuta(rutaOut + File.separator + nombre);
            arch.setFultmod(new Date());
            arch.setUultmod(usuario);
            daoRecepcion.crear(arch, usuario);
        }
        return arch.getNmArchivo();
    }

    /**
     * Metodo para obtener la lista de campos
     * @return La Lista
     */
    public List listaCampos() {
        return estaticasDAO.camposOrden();
    }

    /**
     * Metodo para obtener el total de elementos que cumplen con los criterios
     * @param afiliacion    La afiliacion propietaria
     * @param usuario       Filtro usuario
     * @return Lista de elementos de tipo EventoObj
     */
    public int totalesSinEnviar(final String afiliacion, final String usuario) {
        return daoRecepcion.totalesSinEnviar(afiliacion, usuario);
    }

    /**
     * Metodo para obtener el contenido del catalogo, paginado
     * @param afiliacion    La afiliacion propietaria
     * @param usuario       Filtro usuario
     * @param pagina        El numero de paginas
     * @param bloque        El tamanio el bloque
     * @return Lista de elementos de tipo EventoObj
     */
    public List<ArchivoObj> detallesSinEnviar(final String afiliacion, final String usuario, final int pagina, final int bloque) {
        return daoRecepcion.detallesSinEnviar(afiliacion, usuario, pagina, bloque);
    }

    /**
     * Metodo para transferir un archivo
     * @param propietario   La afiliacion propietaria
     * @param folio         El folio de archivo
     * @param usuario       Filtro usuario
     */
    public void transferirArchivo(final String propietario, final String folio, final String usuario) {
        // OBTIENE LA LISTA DE DETALLES DE KDCOBRO
        List<KdcobroObj>    lista           = daoKdcobro.listar(propietario, folio, usuario);
        List<String>        detalles        = new ArrayList();
        WCobranzaObj        wc              = null;
        PKKgcobroObj        llave2          = new PKKgcobroObj();
        KgcobroObj          general         = null;
        String              cadena          = null;
        PKArchivoObj        llave           = new PKArchivoObj();
        String              nombreArchivo   = null;
        ArchivoObj          archivo         = null;

        llave.setAfiliacion(propietario);
        llave.setCvLista(new Integer(folio));
        archivo = ((ArchivoObj)daoRecepcion.cargar(llave, usuario));
        nombreArchivo = archivo.getNmArchivo();

        // OBTIENE EL DATO GENERAL
        llave2.setAfiliacion(propietario);
        llave2.setFlg(new Integer(folio));
        general = (KgcobroObj)daoKgcobro.cargar(llave2, usuario);

        // OBTIENE LA CADENA QUE SERA EL ENCABEZADO
        cadena = armaEncabezado(general);
        detalles.add(cadena);

        // CONVIERTE LOS DATOS A WCOBRANZA OBJ
        for (KdcobroObj elemento: lista) {
            wc = new WCobranzaObj();
            wc.setAfiliacion(      propietario);
            wc.setCvAfiliacion(    elemento.getCvAfiliacion());
            wc.setCvReferencia(    elemento.getCvReferencia());
            wc.setNoCuenta(        elemento.getNoCuenta());
            wc.setNmCliente(       listasDAO.bancoAfiliacion(propietario, elemento.getCvAfiliacion(), usuario));
            wc.setCvMovimiento(    elemento.getCvMovimiento());
            wc.setMtImporte(       elemento.getMtImporte());
            wc.setFeProgramada(    elemento.getFeProgramada());
            wc.setFeCorte(         elemento.getFeCorte());
            wc.setNumeroContrato(  elemento.getNumeroContrato());

            // OBTIENE LA CADENA DE CADA TRANSACCION Y LA AGREGA A LA LISTA
            detalles.add(armaDetalle(wc));
        }

        // ESCRIBE LOS DETALLES
        String noAfiliacion = utilerias.ajustada(7, utilerias.enteroafiliacion.format(Integer.parseInt(general.getCvAfiliacion())), 1, "0");
        escribeDetalles(nombreArchivo, detalles, noAfiliacion, usuario);

        // ACTUALIZA EL ESTADO DEL ARCHIVO A 03, ENVIADO
        archivo.setStatus("03");
        daoRecepcion.actualizar(archivo, usuario);
    }

    /**
     * Metodo para armar la linea de encabezado a partir de un kgcobro
     * @param general   El dato kgcobro
     * @return La cadena encabezado
     */
    public String armaEncabezado(final KgcobroObj general) {
        String cadena   = null;
        String s1       = utilerias.dateFormatEncabezado.format(new java.sql.Date(general.getFeCorte().getTime()));
        String s2       = utilerias.enteroplano.format(general.getNmTransac());
        String s3       = utilerias.decimalplano.format(general.getGeImporte());

        cadena = s1 + utilerias.ajustada(6, s2, 1, " ") + utilerias.ajustada(16, s3, 1, " ") + utilerias.ajustada(51, " ", 1, " ");
        return cadena;
    }

    /**
     * Metodo para armar la linea de encabezado a partir de un wcobranza
     * @param detalle   El dato wcobranza
     * @return La cadena encabezado
     */
    public String armaDetalle(final WCobranzaObj detalle) {
        String cadena   = null;
        String s1       = utilerias.enteroafiliacion.format(Integer.parseInt(detalle.getCvAfiliacion()));
        String s2       = detalle.getNmCliente().trim();
        String s3       = detalle.getCvReferencia().trim();
        String s4       = detalle.getNoCuenta().trim();
        String s5       = utilerias.decimalplanodetalle.format(detalle.getMtImporte());
        String s6       = detalle.getCvMovimiento();
        String s7       = detalle.getNumeroContrato().trim();

        cadena = utilerias.ajustada(7, s1, 1, "0")  + utilerias.ajustada(2, s2, 0, " ")  + utilerias.ajustada(23, s3, 0, " ") +
                 utilerias.ajustada(19, s4, 0, " ") + utilerias.ajustada(14, s5, 1, "0") + utilerias.ajustada(2, s6, 1, "0") +
                 utilerias.ajustada(20, s7, 0, " ");
                 utilerias.ajustada(51, " ", 1, " ");
        return cadena;
    }

    /**
     * Metodo para armar la linea de encabezado a partir de un kgcobro
     * @param nombre        El nombre del archivo a generar
     * @param detalles      Los detalles a escribir
     * @param afiliacion    La afiliacion a colocar en el archivo inf
     * @param usuario       El usuario que ejecuta
     */
    public void escribeDetalles(final String nombre, final List<String> detalles, final String afiliacion, final String usuario) {
        String              nombreArchivo   = rutaOut + File.separator + nombre;
        FileOutputStream    fos             = null;
        OutputStreamWriter  osw             = null;
        byte[]              aux             = {0x000a};
        String              enter           = new String(aux);

System.out.println("Escribiendo los detalles del archivo:" + nombreArchivo);
        try{
            // ESCRIBE EL ARCHIVO FTP
            fos = new FileOutputStream(nombreArchivo);
            osw = new OutputStreamWriter(fos, "UTF8");

            // ESCRIBE LOS DETALLES
            for(String detalle : detalles){
                osw.write(detalle);
                osw.write(enter);
            }
        } catch (Exception ex) {
           logger.logErrores(nombre, "Error al crear archivo fisico ftp", "BAT01", ex, usuario);
        }finally{
           try{
                if(osw != null){osw.close();}
                if(fos!=null){fos.close();}
           }catch(IOException ex){
               logger.logErrores(nombre, "Error al crear archivo fisico", "BAT01", ex, usuario);
           }
        }

        try{
            // ESCRIBE EL ARCHIVO INF
            nombreArchivo = nombreArchivo.replaceAll(".ftp",".inf");
            fos = new FileOutputStream(nombreArchivo);
            osw = new OutputStreamWriter(fos, "UTF8");
            osw.write(afiliacion);
            osw.write(enter);
        } catch (Exception ex) {
           logger.logErrores(nombre, "Error al crear archivo fisico inf", "BAT01", ex, usuario);
        }finally{
           try{
                if(osw != null){osw.close();}
                if(fos!=null){fos.close();}
           }catch(IOException ex){
               logger.logErrores(nombre, "Error al crear archivo fisico", "BAT01", ex, usuario);
           }
        }
   }

    /**
     * Metodo para transferir un archivo
     * @param propietario   La afiliacion propietaria
     * @param folio         El folio de archivo
     * @param indDup        Indicador de duplicidad
     * @param indRef        Indicador de duplicidad en Referencia
     * @param indCta        Indicador de duplicidad en Cuenta
     * @param indMto        Indicador de duplicidad en Monto
     * @param usuario       Filtro usuario
     */
    public ResultadoValidacionCobranzaObj validaArchivoCobranza(final String propietario, final String folio, final String indDup, final String indRef, final String indCta, final String indMto, final String usuario) {
        // OBTIENE LA LISTA DE DETALLES DE KDCOBRO
        List<KdcobroObj>    lista           = daoKdcobro.listar(propietario, folio, usuario);
        WCobranzaObj        wc              = null;
        PKKgcobroObj        llave2          = new PKKgcobroObj();
        KgcobroObj          general         = null;
        String              cadena          = null;
        PKArchivoObj        llave           = new PKArchivoObj();
        String              nombreArchivo   = null;
        ArchivoObj          archivo         = null;
        Double              minimo          = null;

        ResultadoValidacionCobranzaObj  resultado   = new ResultadoValidacionCobranzaObj();
        RegistroHeaderCobranzaObj       header      = null;
        List<RegistroCuerpoCobranzaObj> detalles2   = new ArrayList();
        RegistroCuerpoCobranzaObj       registro    = null;

        llave.setAfiliacion(propietario);
        llave.setCvLista(new Integer(folio));
        archivo = ((ArchivoObj)daoRecepcion.cargar(llave, usuario));
        nombreArchivo = archivo.getNmArchivo();

        // OBTIENE EL DATO GENERAL
        llave2.setAfiliacion(propietario);
        llave2.setFlg(new Integer(folio));
        general = (KgcobroObj)daoKgcobro.cargar(llave2, usuario);
        minimo = listasDAO.montoBancoAfiliacion(propietario, general.getCvAfiliacion(), usuario);

        // OBTIENE LA CADENA QUE SERA EL ENCABEZADO
        cadena = armaEncabezado(general);
        header = new RegistroHeaderCobranzaObj(cadena);

        int erroresMinimo = 0;
        int erroresMovimiento = 0;
        int erroresAfiliacion = 0;
        int nocatalogados = 0;

        try{

            // CONVIERTE LOS DATOS A WCOBRANZA OBJ
            for (KdcobroObj elemento: lista) {
                wc = new WCobranzaObj();
                wc.setAfiliacion(      propietario);
                wc.setCvAfiliacion(    elemento.getCvAfiliacion());
                wc.setCvReferencia(    elemento.getCvReferencia());
                wc.setNoCuenta(        elemento.getNoCuenta());
                wc.setNmCliente(       listasDAO.bancoAfiliacion(propietario, elemento.getCvAfiliacion(), usuario));
                wc.setCvMovimiento(    elemento.getCvMovimiento());
                wc.setMtImporte(       elemento.getMtImporte());
                wc.setFeProgramada(    elemento.getFeProgramada());
                wc.setFeCorte(         elemento.getFeCorte());
                wc.setNumeroContrato(  elemento.getNumeroContrato());

                if(elemento.getMtImporte() < minimo){ erroresMinimo++;}
                if(Double.parseDouble(elemento.getCvMovimiento()) > 0){ erroresMovimiento++;}
                if(general.getCvAfiliacion().compareTo(elemento.getCvAfiliacion()) != 0){
                    erroresAfiliacion++;}

                // OBTIENE LA CADENA DE CADA TRANSACCION Y LA USA PARA ARMAR UN REGISTRO DE VALIDACION
                registro = new RegistroCuerpoCobranzaObj(armaDetalle(wc));

                PKClienteObj llavecte = new PKClienteObj();
                llavecte.setAfiliacion(propietario);
                llavecte.setCvReferencia(registro.getReferencia());

                PKAfiliacionObj llaveafil = new PKAfiliacionObj();
                llaveafil.setAfiliacion(propietario);
                llaveafil.setCvAfiliacion(registro.getAfiliacion());

                // VALIDA QUE BANCO, AFILIACION Y CLIENTE ESTEN CATALOGADOS
                if (!(servicioBanco.existe(registro.getBanco(), usuario)) && servicioCliente.existe(llavecte, usuario)
                        && servicioAfiliacion.existe(llaveafil, usuario)){
                        nocatalogados++;
                }

                // PONE LA LLAVE PARA VALIDACION DE DUPLICIDAD SEGUN LOS CRITERIOS
                if("1".compareTo(indRef) == 0){
                    if("1".compareTo(indCta) == 0){
                        if("1".compareTo(indMto) == 0){
                            registro.setLlave(registro.getReferencia()+registro.getCuenta()+registro.getImporte());
                        }else{
                            registro.setLlave(registro.getReferencia()+registro.getCuenta());
                        }
                    }else{
                        if("1".compareTo(indMto) == 0){
                            registro.setLlave(registro.getReferencia()+registro.getImporte());
                        }else{
                            registro.setLlave(registro.getReferencia());
                        }
                    }
                }else{
                    if("1".compareTo(indCta) == 0){
                        if("1".compareTo(indMto) == 0){
                            registro.setLlave(registro.getCuenta()+registro.getImporte());
                        }else{
                            registro.setLlave(registro.getCuenta());
                        }
                    }else{
                        if("1".compareTo(indMto) == 0){
                            registro.setLlave(registro.getImporte());
                        }else{
                            registro.setLlave(null);
                        }
                    }
                }
                detalles2.add(registro);
            }

            // DE CADA ELEMENTO DE DETALLES SUMA LA CANTIDAD DE ERRORES REQUERIDOS
            Long erroresRequeridos = detalles2.stream()
                                    .mapToLong(reg -> reg.numErrores())
                                    .sum();

            // CUENTA LOS DE LONGITUD INVALIDA
            Long longitudInvalida = detalles2.size() - detalles2.stream()
                                                 .filter(reg -> reg.longitudValida())
                                                 .count();
            Long duplicados = 0l;
            if("1".compareTo(indRef) == 0){
                // AGRUPA USANDO LA LLAVE, PARA OBTENER LOS QUE SON DUPLICADOS
                Map<String, Long> grupos = detalles2.stream()
                                           .collect(Collectors.groupingBy(RegistroCuerpoCobranzaObj::getLlave, Collectors.counting()));
                duplicados = grupos.entrySet().stream()
                                  .mapToLong(mapa -> mapa.getValue())
                                  .filter(valor -> valor > 1)
                                  .sum();
            }

            // PONE LOS DATOS DE MONTOS, TRANSACCIONES Y DIFERENCIAS
            Double montoDetalles = detalles2.stream()
                                   .mapToDouble(reg -> Double.parseDouble(reg.getImporte()))
                                   .sum();
            resultado.setNombreArchivo(     nombreArchivo);
            resultado.setTipoArchivo(       "Nacional");
            resultado.setAfiliacion(        general.getCvAfiliacion());
            resultado.setErroresEncabezado( header.numErrores());
            resultado.setLongitudDiferente( longitudInvalida.intValue());
            resultado.setMontoMenor(        erroresMinimo);
            resultado.setMovimientos(       erroresMovimiento);
            resultado.setAfiliaciones(      erroresAfiliacion);
            resultado.setNocatalogados(     nocatalogados);
            resultado.setRequeridosC(       erroresRequeridos.intValue());
            resultado.setDuplicadas(        duplicados.intValue());
            resultado.setMontoHeader(       Double.parseDouble(header.getMonto()));
            resultado.setMontoDetalles(     montoDetalles);
            resultado.setTrxHeader(         Integer.parseInt(header.getTransacciones().trim()));
            resultado.setTrxDetalles(       detalles2.size());
            resultado.setMontoDiferencia(   Math.abs(resultado.getMontoHeader() - resultado.getMontoDetalles()));
            resultado.setTrxDiferencia(     Math.abs(resultado.getTrxHeader()   - resultado.getTrxDetalles()));

            // SI CUALQUIERA DE LOS DATOS A VALIDAR TIENE CONTENIDO, EL RESULTADO ES FALSO, EN OTRO CASO VERDADERO
            if(resultado.getErroresEncabezado() > 0 || resultado.getLongitudDiferente() > 0 || resultado.getRequeridosC() > 0 ||
               resultado.getDuplicadas() > 0        || resultado.getMontoDiferencia() > 0   || resultado.getTrxDiferencia() > 0 ||
               resultado.getMontoMenor() > 0        || resultado.getMovimientos() > 0       || resultado.getAfiliaciones() > 0){
                resultado.setResultado(false);
                // ACTUALIZA EL ESTADO DEL ARCHIVO A 02, INVALIDO
                archivo.setStatus("02");
                daoRecepcion.actualizar(archivo, usuario);
            }else{
                resultado.setResultado(true);
                // ACTUALIZA EL ESTADO DEL ARCHIVO A 01, VALIDO
                archivo.setStatus("01");
                daoRecepcion.actualizar(archivo, usuario);
            }
        } catch (Exception ex) {
            logger.logErrores(nombre, "Al validar Archivo Cobranza", "BAN01", ex, "sys");
        }

        // REALIZA LA VALIDACION DEL ARCHIVO
        return resultado;
    }

    /**
     * Metodo para obtener el total de elementos que cumplen con los criterios
     * @param afiliacion    La afiliacion propietaria
     * @param usuario       Filtro usuario
     * @return Lista de elementos de tipo EventoObj
     */
    public int totalesSinValidar(final String afiliacion, final String usuario){
        return daoRecepcion.totalesSinValidar(afiliacion, usuario);
    }

    /**
     * Metodo para obtener el contenido del catalogo sin validar, paginado
     * @param afiliacion    La afiliacion propietaria
     * @param usuario       Filtro usuario
     * @param pagina        El numero de paginas
     * @param bloque        El tamanio el bloque
     * @return Lista de elementos de tipo EventoObj
     */
    public List<ArchivoObj> detallesSinValidar(final String afiliacion, final String usuario, final int pagina, final int bloque){
        return daoRecepcion.detallesSinValidar(afiliacion, usuario, pagina, bloque);
    }
}
