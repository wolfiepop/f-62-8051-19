// ----------------------------------------------------------------------------
// Nombre del Programa : ConfiguracionDAO
// Autor               : Manuel Villalobos
// Compania            : GSOF
// Proyecto/Procliente : D-52-8272-17                    Fecha: 02/ABR/2018
// Descripcion General : CLASE QUE APLICA OPERACIONES BD DE DATOS DE CONFIGURACION
// Programa Dependiente: N/A
// Programa Subsecuente: N/A
// Cond. de ejecucion  : N/A
// Dias de ejecucion   : N/A                                 Horario: N/A
//                              MODIFICACIONES
// ----------------------------------------------------------------------------
// Numero de Parametros:
// Parametros Entrada  :                                    Formato:
//
// Parametros Salida   : N/A                                Formato: N/A
// ----------------------------------------------------------------------------

package mx.com.prosa.wsca.modelo;

import mx.com.prosa.wsca.beans.*;
import mx.com.prosa.wsca.util.*;
import java.sql.*;
import java.util.*;
import org.hibernate.*;
import org.hibernate.cfg.*;
import org.hibernate.type.StandardBasicTypes;

/**
 * Clase DAO para las operaciones de Base de Datos de Datos de Configuracion
 * @author Manuel Villalobos (GSOF)
 * @version 02/ABR/2018
 */
public class ConfiguracionDAO extends GenericoDAO<ConfiguracionObj> implements IConfiguracionDAO<ConfiguracionObj> {
    private static          ConfiguracionDAO  instancia   = null;

    /**
     * Constructor privado
     */
    private ConfiguracionDAO() {
        setNombre("Configuracion");
    }

    /**
     * Metodo para obtener la instancia del Singleton
     * @return La instancia del singleton
     */
    public static ConfiguracionDAO getInstancia() {
        if(instancia == null) {
            instancia = new ConfiguracionDAO();
        }

        return instancia;
    }

    /**
     * Metodo para obtener el contenido del catalogo
     * @param usuario  El usuario que ejecuta el metodo
     * @return La lista de elementos
     */
    public List listar(final String usuario) {
        Session     session = HibernateUtil.getSession();
        List        lista   = null;
        Query       query   = null;

        try {
            query = session.createQuery("from ConfiguracionObj as b order by b.afiliacion asc");
            lista = consultar(query);
        } catch (HibernateException ex) {
            claveError = BD002;
            logger.logErrores(nombre, ELISTAR, claveError, ex, usuario);
            lista = new ArrayList();
        }finally{
            session.close();
        }
        return lista;
    }

    /**
     * Metodo para validar la existencia un registro
     * @param llave     La llave del registro
     * @param usuario   El usuario que ejecuta el metodo
     * @return Si existe o no
     */
    public boolean existe(final String llave, final String usuario) {
       Session     session = HibernateUtil.getSession();
       Boolean     regreso = false;
       Query       query   = null;

       try {
           String sentencia = "Select count(1) as numero From sca.configuracion where afiliacion like ?";
           query = session.createSQLQuery(sentencia)
                          .addScalar("numero",       StandardBasicTypes.LONG)
                          .setString(0, llave);
           if(conteo(query) > 0) regreso = true;
       } catch (HibernateException ex) {
           claveError = BD008;
           logger.logErrores(nombre, EEXISTENCIA, claveError, ex, usuario);
           regreso = false;
       }finally{
           session.close();
       }

       return regreso;
    }

    /**
     * Metodo para actualizar el registro de un usuario
     * @param llave             La afiliacion propietaria
     * @param usuario           El usuario que ejecuta el metodo
     * @return El resultado
     */
    public boolean actualizaFolio(final String llave, final String usuario) {
        Session session         = HibernateUtil.newSession();
        boolean regreso         = false;
        Query   query           = null;
        String  sentencia       = "update sca.configuracion set folio = folio + 1 where afiliacion = :llave";

       try {
           query = session.createSQLQuery(sentencia)
                          .setString("llave",               llave);
           aplicarComando(query);
           regreso = true;
       } catch (HibernateException ex) {
           claveError = BD003;
           logger.logErrores(nombre, EACTUALIZAR, claveError, ex, usuario);
           regreso = false;
       }finally{
           session.close();
       }

       return regreso;
    }

    /**
     * Metodo para incrementar en uno el folio de una afiliacion
     * @param propietario   La afiliacion propietaria
     * @param usuario       El usuario que ejecuta el metodo
     * @return Si se actualiza correctamente
     */
    public boolean actualizarFolio(final String propietario, final String usuario){
        Session session         = HibernateUtil.newSession();
        boolean regreso         = false;
        Query   query           = null;
        String  sentencia       = "update sca.configuracion set folio = decode(folio, 100, 0, folio + 1), fultmod = sysdate, pultmod = :programa " +
                                  "where afiliacion = :propietario";
       try {
           query = session.createSQLQuery(sentencia)
                          .setString("programa",    "WSCA")
                          .setString("propietario", propietario);
           aplicarComando(query);
           regreso = true;
       } catch (HibernateException ex) {
           claveError = BD003;
           logger.logErrores(nombre, EACTUALIZAR, claveError, ex, usuario);
           regreso = false;
       }finally{
           session.close();
       }

       return regreso;
    }

    /**
     * Metodo para insertar el registro de una afiliacion
     * @param propietario   La afiliacion propietaria
     * @param fiid          La fiid que le corresponde
     * @param usuario       El usuario que ejecuta el metodo
     * @return Si se actualiza correctamente
     */
    public boolean insertarRegistro(final String propietario, final String fiid, final String usuario){
        Session session         = HibernateUtil.newSession();
        boolean regreso         = false;
        Query   query           = null;
        Long    id              = null;
        String  sentencia       = "insert into sca.configuracion(afiliacion, medio, folio, fiid, identificador) " +
                                  "values(:propietario, '1', 0, :fiid, :id)";
        String  sentencia2      = "select to_number(max(identificador)) + 1 as numero from sca.configuracion";
       try {
           query = session.createSQLQuery(sentencia2)
                          .addScalar("numero",       StandardBasicTypes.LONG);
           id = conteo(query);

           query = session.createSQLQuery(sentencia)
                          .setString("fiid",        fiid)
                          .setString("id",          utilerias.entero00.format(id))
                          .setString("propietario", propietario);
           aplicarComando(query);
           regreso = true;
       } catch (HibernateException ex) {
           claveError = BD003;
           logger.logErrores(nombre, EACTUALIZAR, claveError, ex, usuario);
           regreso = false;
       }finally{
           session.close();
       }

       return regreso;
    }

}
