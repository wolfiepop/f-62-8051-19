// ----------------------------------------------------------------------------
// Nombre del Programa : RecepcionDAO
// Autor               : Manuel Villalobos
// Compania            : GSOF
// Proyecto/Procliente : D-52-8272-17                    Fecha: 02/ABR/2018
// Descripcion General : CLASE QUE APLICA OPERACIONES BD DE RECEPCION
// Programa Dependiente: N/A
// Programa Subsecuente: N/A
// Cond. de ejecucion  : N/A
// Dias de ejecucion   : N/A                                 Horario: N/A
//                              MODIFICACIONES
// ----------------------------------------------------------------------------
// Numero de Parametros:
// Parametros Entrada  :                                    Formato:
//
// Parametros Salida   : N/A                                Formato: N/A
// ----------------------------------------------------------------------------

package mx.com.prosa.wsca.modelo;

import mx.com.prosa.wsca.beans.*;
import mx.com.prosa.wsca.util.*;
import java.sql.*;
import java.util.*;
import org.hibernate.*;
import org.hibernate.cfg.*;
import org.hibernate.type.StandardBasicTypes;

/**
 * Clase DAO para las operaciones de Base de Datos del catalogo de Recepcion
 * @author Manuel Villalobos (GSOF)
 * @version 02/ABR/2018
 */
public class RecepcionDAO extends GenericoDAO<ArchivoObj> implements IRecepcionDAO<ArchivoObj> {
    private static          RecepcionDAO  instancia   = null;

    /**
     * Constructor privado
     */
    private RecepcionDAO() {
        setNombre("Recepciones");
    }

    /**
     * Metodo para obtener la instancia del Singleton
     * @return La instancia del singleton
     */
    public static RecepcionDAO getInstancia() {
        if(instancia == null) {
            instancia = new RecepcionDAO();
        }

        return instancia;
    }

    /**
     * Metodo para obtener el contenido del catalogo de una afiliacion
     * @param afiliacion    La afiliacion propietaria
     * @param usuario       El usuario que ejecuta el metodo
     * @return La lista de elementos
     */
    public List<ArchivoObj> listar(final String afiliacion, final String usuario) {
        Session     session = HibernateUtil.getSession();
        List        lista   = null;
        Query       query   = null;

        try {
            query = session.createQuery("from ArchivoObj as b where b.llave.afiliacion = :afiliacion order by b.llave.cvLista asc")
                                        .setString("afiliacion", afiliacion);
            lista = consultar(query);
        } catch (HibernateException ex) {
            claveError = BD002;
            logger.logErrores(nombre, ELISTAR, claveError, ex, usuario);
            lista = new ArrayList();
        }finally{
            session.close();
        }
        return lista;
    }

    /**
     * Metodo para obtener el contenido del catalogo de una afiliacion
     * @param afiliacion    La afiliacion propietaria
     * @param usuario       El usuario que ejecuta el metodo
     * @return La lista de elementos
     */
    public List<ArchivoObj> listarSinEnviar(final String afiliacion, final String usuario) {
        Session     session = HibernateUtil.getSession();
        List        lista   = null;
        Query       query   = null;

        try {
            query = session.createQuery("from ArchivoObj as b where b.llave.afiliacion = :afiliacion and b.status in ('00','01','02') order by b.llave.cvLista asc")
                                        .setString("afiliacion", afiliacion);
            lista = consultar(query);
        } catch (HibernateException ex) {
            claveError = BD002;
            logger.logErrores(nombre, ELISTAR, claveError, ex, usuario);
            lista = new ArrayList();
        }finally{
            session.close();
        }
        return lista;
    }

    /**
     * Metodo para obtener el total de elementos que cumplen con los criterios
     * @param afiliacion    La afiliacion propietaria
     * @param usuario       Filtro usuario
     * @return Lista de elementos de tipo EventoObj
     */
    public int totalesSinEnviar(final String afiliacion, final String usuario) {
        Session     session = HibernateUtil.getSession();
        Long        result  = null;
        Query       query   = null;

        String sentencia = "Select count(1) from ArchivoObj as b where b.llave.afiliacion = :afiliacion " +
                            "and b.status in ('00','01','02')";

        try {
            query = session.createQuery(sentencia)
                                        .setString("afiliacion", afiliacion);
            result = conteo(query);
        } catch (HibernateException ex) {
            claveError = BD002;
            logger.logErrores(nombre, "Error al obtener totales", claveError, ex, usuario);
            result = null;
        }finally{
            session.close();
        }

        return result.intValue();
    }

    /**
     * Metodo para obtener el contenido del catalogo, paginado
     * @param afiliacion    La afiliacion propietaria
     * @param usuario       Filtro usuario
     * @param pagina        El numero de paginas
     * @param bloque        El tamanio el bloque
     * @return Lista de elementos de tipo EventoObj
     */
    public List<ArchivoObj> detallesSinEnviar(final String afiliacion, final String usuario, final int pagina, final int bloque) {
        Session     session = HibernateUtil.getSession();
        Query       query   = null;
        List        result  = null;

        String sentencia = "from ArchivoObj as b where b.llave.afiliacion = :afiliacion and b.status in ('00','01','02') order by b.llave.cvLista";

        try {
            query = session.createQuery(sentencia)
                            .setString("afiliacion", afiliacion)
                            .setFirstResult(pagina)
                            .setMaxResults(bloque);
            result = consultar(query);
        } catch (HibernateException ex) {
            claveError = BD002;
            logger.logErrores(nombre, ELISTAR, claveError, ex, usuario);
            result = new ArrayList();
        }finally{
            session.close();
        }
        return result;
    }


    /**
     * Metodo para obtener el total de elementos que cumplen con los criterios
     * @param afiliacion    La afiliacion propietaria
     * @param usuario       Filtro usuario
     * @return Lista de elementos de tipo EventoObj
     */
    public int totales(final String afiliacion, final String usuario) {
        Session     session = HibernateUtil.getSession();
        Long        result  = null;
        Query       query   = null;

        String sentencia = "Select count(1) from ArchivoObj as b where b.llave.afiliacion = :afiliacion";

        try {
            query = session.createQuery(sentencia)
                                        .setString("afiliacion", afiliacion);
            result = conteo(query);
        } catch (HibernateException ex) {
            claveError = BD002;
            logger.logErrores(nombre, "Error al obtener totales", claveError, ex, usuario);
            result = null;
        }finally{
            session.close();
        }

        return result.intValue();
    }

    /**
     * Metodo para obtener el contenido del catalogo, paginado
     * @param afiliacion    La afiliacion propietaria
     * @param usuario       Filtro usuario
     * @param pagina        El numero de paginas
     * @param bloque        El tamanio el bloque
     * @return Lista de elementos de tipo EventoObj
     */
    public List<ArchivoObj> detalles(final String afiliacion, final String usuario, final int pagina, final int bloque) {
        Session     session = HibernateUtil.getSession();
        Query       query   = null;
        List        result  = null;

        String sentencia = "from ArchivoObj as b where b.llave.afiliacion = :afiliacion order by b.llave.cvLista";

        try {
            query = session.createQuery(sentencia)
                            .setString("afiliacion", afiliacion)
                            .setFirstResult(pagina)
                            .setMaxResults(bloque);
            result = consultar(query);
        } catch (HibernateException ex) {
            claveError = BD002;
            logger.logErrores(nombre, ELISTAR, claveError, ex, usuario);
            result = new ArrayList();
        }finally{
            session.close();
        }
        return result;
    }

    /**
     * Metodo para validar la existencia un registro
     * @param llave     La llave del registro
     * @param usuario   El usuario que ejecuta el metodo
     * @return Si existe o no
     */
    public boolean existe(final PKArchivoObj llave, final String usuario) {
       Session     session = HibernateUtil.getSession();
       Boolean     regreso = false;
       Query       query   = null;
System.out.println("Buscando archivo:" + llave.getAfiliacion() + "-" +  llave.getCvLista().toString());
       try {
           String sentencia = "Select count(1) as numero From sca.archivos where afiliacion like :afiliacion and cv_lista = :flg and status in ('00','01','02')";
           query = session.createSQLQuery(sentencia)
                          .addScalar("numero",        StandardBasicTypes.LONG)
                          .setString("afiliacion",    llave.getAfiliacion())
                          .setString("flg",           llave.getCvLista().toString());
           if(conteo(query) > 0) regreso = true;
       } catch (HibernateException ex) {
           claveError = BD008;
           logger.logErrores(nombre, EEXISTENCIA, claveError, ex, usuario);
           regreso = false;
       }finally{
           session.close();
       }

       return regreso;
    }

    /**
     * Metodo para validar la existencia un registro
     * @param llave     La llave del registro
     * @param usuario   El usuario que ejecuta el metodo
     * @return Si existe o no
     */
    public boolean existeSinEnviar(final PKArchivoObj llave, final String usuario) {
       Session     session = HibernateUtil.getSession();
       Boolean     regreso = false;
       Query       query   = null;

       try {
           String sentencia = "Select count(1) as numero From sca.archivos where afiliacion like :afiliacion and cv_lista = :flg " +
                                "and status in ('01','02','00')";
           query = session.createSQLQuery(sentencia)
                          .addScalar("numero",        StandardBasicTypes.LONG)
                          .setString("afiliacion",    llave.getAfiliacion())
                          .setString("flg",           llave.getCvLista().toString());
           if(conteo(query) > 0) regreso = true;
       } catch (HibernateException ex) {
           claveError = BD008;
           logger.logErrores(nombre, EEXISTENCIA, claveError, ex, usuario);
           regreso = false;
       }finally{
           session.close();
       }

       return regreso;
    }

    /**
     * Metodo para obtener el total de elementos que cumplen con los criterios
     * @param afiliacion    La afiliacion propietaria
     * @param usuario       Filtro usuario
     * @return Lista de elementos de tipo EventoObj
     */
    public int totalesSinValidar(final String afiliacion, final String usuario) {
        Session     session = HibernateUtil.getSession();
        Long        result  = null;
        Query       query   = null;

        String sentencia = "Select count(1) from ArchivoObj as b where b.llave.afiliacion = :afiliacion " +
                            "and b.status in ('00')";

        try {
            query = session.createQuery(sentencia)
                                        .setString("afiliacion", afiliacion);
            result = conteo(query);
        } catch (HibernateException ex) {
            claveError = BD002;
            logger.logErrores(nombre, "Error al obtener totales", claveError, ex, usuario);
            result = null;
        }finally{
            session.close();
        }

        return result.intValue();
    }

    /**
     * Metodo para obtener el contenido del catalogo sin validar, paginado
     * @param afiliacion    La afiliacion propietaria
     * @param usuario       Filtro usuario
     * @param pagina        El numero de paginas
     * @param bloque        El tamanio el bloque
     * @return Lista de elementos de tipo EventoObj
     */
    public List<ArchivoObj> detallesSinValidar(final String afiliacion, final String usuario, final int pagina, final int bloque) {
        Session     session = HibernateUtil.getSession();
        Query       query   = null;
        List        result  = null;

        String sentencia = "from ArchivoObj as b where b.llave.afiliacion = :afiliacion and b.status in ('00') order by b.llave.cvLista";

        try {
            query = session.createQuery(sentencia)
                            .setString("afiliacion", afiliacion)
                            .setFirstResult(pagina)
                            .setMaxResults(bloque);
            result = consultar(query);
        } catch (HibernateException ex) {
            claveError = BD002;
            logger.logErrores(nombre, ELISTAR, claveError, ex, usuario);
            result = new ArrayList();
        }finally{
            session.close();
        }
        return result;
    }
}
