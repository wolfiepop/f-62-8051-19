// ----------------------------------------------------------------------------
// Nombre del Programa : ConfiguracionServicio
// Autor               : Manuel Villalobos
// Compania            : GSOF
// Proyecto/Procliente : D-52-8272-17                    Fecha: 02/ABR/2018
// Descripcion General : CLASE SERVICIO PARA CONFIGURACION
// Programa Dependiente: N/A
// Programa Subsecuente: N/A
// Cond. de ejecucion  : N/A
// Dias de ejecucion   : N/A                                 Horario: N/A
//                              MODIFICACIONES
// ----------------------------------------------------------------------------
// Numero de Parametros:
// Parametros Entrada  :                                    Formato:
//
// Parametros Salida   : N/A                                Formato: N/A
// ----------------------------------------------------------------------------

package mx.com.prosa.wsca.servicio;

import mx.com.prosa.wsca.beans.*;
import mx.com.prosa.wsca.modelo.*;
import mx.com.prosa.wsca.util.*;
import mx.com.prosa.wsca.fabrica.*;
import java.util.*;

/**
 * Clase Servicio para Configuracion
 * @author Manuel Villalobos (GSOF)
 * @version 02/ABR/2018
 */
public class ConfiguracionServicio extends BaseServicio implements IConfiguracionServicio {
    private static  ConfiguracionServicio       instancia   = null;
    private         IConfiguracionDAO           dao         = ConfiguracionFabrica.getDAO();

    /**
     * Constructor privado
     */
    private ConfiguracionServicio() {
        setNombre("Configuracion");
    }

    /**
     * Metodo para obtener la instancia del Singleton
     * @return La instancia del singleton
     */
    public static ConfiguracionServicio getInstancia() {
        if(instancia == null) {
            instancia = new ConfiguracionServicio();
        }

        return instancia;
    }

    /**
     * Metodo para crear un registro
     * @param datos  Los datos del registro
     * @param usuario  El usuario que ejecuta el metodo
     * @return Si se crea correctamente
     */
    public boolean crear(final ConfiguracionObj datos, final String usuario){
        // INVOCA LA ACCION EN EL DAO, DEVUELVE LO QUE DA EL DAO
        return dao.crear(datos, usuario);
    }

    /**
     * Metodo para cargar un registro
     * @param llave    La llave del registro
     * @param usuario  El usuario que ejecuta el metodo
     * @return El registro
     */
    public ConfiguracionObj cargar(final String llave, final String usuario){
System.out.println("Cargando configuracion de:" + llave);
        return (ConfiguracionObj)dao.cargar(llave, usuario);
    }

    /**
     * Metodo para actualizar un registro
     * @param datos    Los datos del registro
     * @param usuario  El usuario que ejecuta el metodo
     * @return Si se actualiza correctamente
     */
    public boolean actualizar(final ConfiguracionObj datos, final String usuario){
        datos.setFultmod(  utilerias.calendarAFecha(utilerias.fechaActual()));
        datos.setUultmod(  usuario);
        datos.setPultmod(  "WSCA");

        // INVOCA LA ACCION EN EL DAO, DEVUELVE LO QUE DA EL DAO
        return dao.actualizar(datos, usuario);
    }

    /**
     * Metodo para obtener el contenido del catalogo
     * @param usuario  El usuario que ejecuta el metodo
     * @return La lista de elementos
     */
    public List listar(final String usuario){
        // INVOCA LA ACCION EN EL DAO, DEVUELVE LO QUE DA EL DAO
        return dao.listar(usuario);
    }

    /**
     * Metodo para validar la existencia un registro
     * @param llave     La llave del registro
     * @param usuario   El usuario que ejecuta el metodo
     * @return Si existe o no
     */
    public boolean existe(final String llave, final String usuario){
        // INVOCA LA ACCION EN EL DAO, DEVUELVE LO QUE DA EL DAO
        return dao.existe(llave, usuario);
    }

    /**
     * Metodo para actualizar el registro de un usuario
     * @param llave             La afiliacion propietaria
     * @param usuario           El usuario que ejecuta el metodo
     * @return El resultado
     */
    public boolean actualizaFolio(final String llave, final String usuario){
        return dao.actualizaFolio(llave, usuario);
    }

    /**
     * Metodo para insertar el registro de una afiliacion
     * @param propietario   La afiliacion propietaria
     * @param fiid          La fiid que le corresponde
     * @param usuario       El usuario que ejecuta el metodo
     * @return Si se actualiza correctamente
     */
    public boolean insertarRegistro(final String propietario, final String fiid, final String usuario){
        return dao.insertarRegistro(propietario, fiid, usuario);
    }
}
