// ----------------------------------------------------------------------------
// Nombre del Programa : IConfiguracionDAO
// Autor               : Manuel Villalobos
// Compania            : GSOF
// Proyecto/Procliente : D-52-8272-17                    Fecha: 02/ABR/2018
// Descripcion General : INTERFAZ DEL DAO DE CONFIGURACION
// Programa Dependiente: N/A
// Programa Subsecuente: N/A
// Cond. de ejecucion  : N/A
// Dias de ejecucion   : N/A                                 Horario: N/A
//                              MODIFICACIONES
// ----------------------------------------------------------------------------
// Numero de Parametros:
// Parametros Entrada  :                                    Formato:
//
// Parametros Salida   : N/A                                Formato: N/A
// ----------------------------------------------------------------------------

package mx.com.prosa.wsca.modelo;

import java.util.List;

/**
 * Interfaz del DAO de Configuracion
 * @author Manuel Villalobos (GSOF)
 * @version 02/ABR/2018
 */
public interface IConfiguracionDAO<T> extends IGenericoDAO<T> {
    /**
     * Metodo para obtener el contenido del catalogo
     * @param usuario  El usuario que ejecuta el metodo
     * @return La lista de elementos
     */
    public List listar(final String usuario);

    /**
     * Metodo para validar la existencia un registro
     * @param llave     La llave del registro
     * @param usuario   El usuario que ejecuta el metodo
     * @return Si existe o no
     */
    public boolean existe(final String llave, final String usuario);

    /**
     * Metodo para actualizar el registro de un usuario
     * @param llave             La afiliacion propietaria
     * @param usuario           El usuario que ejecuta el metodo
     * @return El resultado
     */
    public boolean actualizaFolio(final String llave, final String usuario);

    /**
     * Metodo para incrementar en uno el folio de una afiliacion
     * @param propietario   La afiliacion propietaria
     * @param usuario       El usuario que ejecuta el metodo
     * @return Si se actualiza correctamente
     */
    public boolean actualizarFolio(final String propietario, final String usuario);

    /**
     * Metodo para insertar el registro de una afiliacion
     * @param propietario   La afiliacion propietaria
     * @param fiid          La fiid que le corresponde
     * @param usuario       El usuario que ejecuta el metodo
     * @return Si se actualiza correctamente
     */
    public boolean insertarRegistro(final String propietario, final String fiid, final String usuario);
}